FROM openjdk:17.0.2-jdk

EXPOSE 8080

RUN mkdir -p /usr/app

COPY ./build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

ENTRYPOINT ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]
